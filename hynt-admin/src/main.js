// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import Router from 'vue-router'

import Vuex from 'vuex'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import '@/common/base/font/iconfont/iconfont.css'
import("babel-polyfill")
import store from '@/store/store'
import VueBlu from 'vue-blu'
import 'vue-blu/dist/css/vue-blu.min.css'

Vue.use(VueBlu)
Vue.use(ElementUI)
Vue.use(Vuex)
Vue.config.productionTip = false
//Vue.prototype.$store = store
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})

const originalPush = Router.prototype.push
Router.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err)
}
//路由拦截
router.beforeEach((to,from,next) => {
	console.log(to.fullPath)
//	console.log(store)
    if(to.matched.some( m => m.meta.auth)){
        // 对路由进行验证
        if(store.getters.isLogin === true) { // 已经登陆
            next()     // 正常跳转到你设置好的页面
        }else{
//      	if(router.currentRoute.name == "target"){
        		// 未登录则跳转到登陆界面，query:{ Rurl: to.fullPath}表示把当前路由信息传递过去方便登录后跳转回来；
　　 　　　　next({path:'/',query:{ redirect: to.fullPath} })
//      	}else{
//      		console.log(200)
//      		router.push('/index');
//      	}
            
   　　　　　} 
　　　　}else{ 
　　　　　　next() 
　　} 
})