export default[
    {
      path: '/index',
      name: 'Index',
      component: resolve=>(require(["@/pages/home/index"],resolve)),
      children: [  //这里就是二级路由的配置
	        {
	          path: '/fromstructure',
	          name: 'Fromstructure',
	          meta:{auth:true,keepAlive: false}, // 不需要缓存ture需要缓存
	          component:resolve=>(require(["@/pages/from-permission/from-structure"],resolve)),
	        },  
	        {
	          path: '/powerstructure',
	          name: 'Powerstructure',
	          meta:{auth:true,keepAlive: false}, // 不需要缓存ture需要缓存
	          component:resolve=>(require(["@/pages/power-permission/power-struct"],resolve)),
	        }, 
	         {
	           path: '/paper/list',
	           name: 'paperList',
	           meta:{auth:true,keepAlive: false}, // 不需要缓存ture需要缓存
	           component:resolve=>(require(["@/pages/exam/paper/list"],resolve)),
	         }, 
	         {
	           path: '/paper/edit',
	           name: 'paperEdit',
	           meta:{auth:true,keepAlive: false}, // 不需要缓存ture需要缓存
	           component:resolve=>(require(["@/pages/exam/paper/edit"],resolve)),
	         }, 
	        {
	          path: '/question/list',
	          name: 'questionList',
	          meta:{auth:false,keepAlive: false}, // 不需要缓存ture需要缓存
	          component:resolve=>(require(["@/pages/exam/question/list"],resolve)),
	        }, 
	         {
	          path: '/exam/question/edit/singleChoice',
	          name: 'questionList',
	          meta:{auth:false,keepAlive: false}, // 不需要缓存ture需要缓存
	          component:resolve=>(require(["@/pages/exam/question/edit/single-choice"],resolve)),
	        }, 
	         {
	          path: '/exam/question/edit/multipleChoice',
	          name: 'questionList',
	          meta:{auth:false,keepAlive: false}, // 不需要缓存ture需要缓存
	          component:resolve=>(require(["@/pages/exam/question/edit/multiple-choice"],resolve)),
	        }, 
	        {
	          path: '/exam/question/edit/trueFalse',
	          name: 'questionList',
	          meta:{auth:false,keepAlive: false}, // 不需要缓存ture需要缓存
	          component:resolve=>(require(["@/pages/exam/question/edit/true-false"],resolve)),
	        }, 
	        {
	          path: '/exam/question/edit/gapFilling',
	          name: 'questionList',
	          meta:{auth:false,keepAlive: false}, // 不需要缓存ture需要缓存
	          component:resolve=>(require(["@/pages/exam/question/edit/gap-filling"],resolve)),
	        }, 
	        {
	          path: '/exam/question/edit/shortAnswer',
	          name: 'questionList',
	          meta:{auth:false,keepAlive: false}, // 不需要缓存ture需要缓存
	          component:resolve=>(require(["@/pages/exam/question/edit/short-answer"],resolve)),
	        }, 
	    ]
    },
    
  ]
