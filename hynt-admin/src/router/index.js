import Vue from 'vue'
import Router from 'vue-router'
import Index from './main'

Vue.use(Router)

export default new Router({
  routes: [
  	...Index,
  	{
      path: '/',
      name: 'Login',
      component: resolve=>(require(["@/pages/login/indexl"],resolve))
    },	
  ]
})
