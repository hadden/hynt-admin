
import serviceAxios from '@/common/config/axios'

/*
 请求serviceAxios
 * */

//获取所有人
export function alluser(data){
	return serviceAxios({
	    url: '/sys/user/all',
	    method: 'GET',
	    params: data
  	})
}