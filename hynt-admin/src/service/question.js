import serviceAxios from '@/common/config/axios'

/*
 登录请求serviceAxios
 * */

export function pageList(data){
	return serviceAxios({
	    url: '/admin/question/page',
	    method: 'POST',
	    data
  	})
}

export function edit(data){
	return serviceAxios({
	    url: '/admin/question/edit',
	    method: 'POST',
	    data
  	})
}
//退出登录
export function select(data){
	return serviceAxios({
	    url: '/admin/question/select/'+data,
	    method: 'POST'
  	})
}
export function deleteQuestion(data){
	return serviceAxios({
	    url: '/admin/question/delete/'+data,
	    method: 'POST'
  	})
}