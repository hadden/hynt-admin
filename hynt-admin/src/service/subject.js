
import serviceAxios from '@/common/config/axios'

/*
 登录请求serviceAxios
 * */

function list(data){
	return serviceAxios({
	    url: '/admin/education/subject/list',
	    method: 'POST',
	    data
  	})
}
function pageList(data){
	return taskExamPage({
	    url: '/admin/education/subject/page',
	    method: 'POST',
	    data
  	})
}
function edit(data){
	return serviceAxios({
	    url: '/admin/education/subject/edit',
	    method: 'POST',
	    data
  	})
}
function select(data){
	return serviceAxios({
	    url: '/api/admin/education/subject/select/'+data,
	    method: 'POST'
  	})
}
export default {
	list:list,
  pageList: pageList,
  edit: edit,
  select: select
}
