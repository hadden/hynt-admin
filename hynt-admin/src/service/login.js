import serviceAxios from '@/common/config/axios'

/*
 登录请求serviceAxios
 * */

export function login(data){
	return serviceAxios({
	    url: '/login',
	    method: 'POST',
	    data
  	})
}
//退出登录
export function outlogin(data){
	return serviceAxios({
	    url: '/logout',
	    method: 'GET',
	    params:data
  	})
}