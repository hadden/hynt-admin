import serviceAxios from '@/common/config/axios'

/*
 登录请求serviceAxios
 * */
//export default {
//pageList: query => post('/api/admin/exam/paper/page', query),
//taskExamPage: query => post('/api/admin/exam/paper/taskExamPage', query),
//edit: query => post('/api/admin/exam/paper/edit', query),
//select: id => post('/api/admin/exam/paper/select/' + id),
//deletePaper: id => post('/api/admin/exam/paper/delete/' + id)
//}
function pageList(data){
	return serviceAxios({
	    url: '/admin/exam/paper/page',
	    method: 'POST',
	    data
  	})
}
function taskExamPage(data){
	return serviceAxios({
	    url: '/admin/exam/paper/taskExamPage',
	    method: 'POST',
	    data
  	})
}
export function tpageList(data){
	return serviceAxios({
	    url: '/admin/question/page',
	    method: 'POST',
	    data
  	})
}
function selectPaper(data){
	return serviceAxios({
	    url: '/admin/exam/paper/select/'+data,
	    method: 'POST'
  	})
}
function edit(data){
	return serviceAxios({
	    url: '/admin/exam/paper/edit',
		method: 'POST',
		data
  	})
}
function deletePaper(data){
	return serviceAxios({
	    url: '/admin/exam/paper/delete/'+data,
	    method: 'POST'
  	})
}
export default {
  pageList: pageList,
  taskExamPage: taskExamPage,
  edit: edit,
  tpageList:tpageList,
  selectPaper: selectPaper,
  deletePaper: deletePaper
}