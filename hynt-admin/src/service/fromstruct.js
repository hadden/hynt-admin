
import serviceAxios from '@/common/config/axios'

/*
 请求serviceAxios
 * */

export function treemenulist(data){
	return serviceAxios({
	    url: '/system/sysDept/tree',
	    method: 'GET',
	    params: data
  	})
}
//id获取
export function menuIdlist(data){
	return serviceAxios({
	    url: '/sys/user/list',
	    method: 'GET',
	    params: data
  	})
}
//职位查询
export function worklist(data){
	return serviceAxios({
	    url: '/user/level/list',
	    method: 'GET',
	    params: data
  	})
}
//excel数据导出
export function savelocalexcel(data){
	return serviceAxios({
	    url: '/sys/user/saveUserExcel',
	    method: 'GET',
	    params: data
  	})
}
//批量删除数据
export function selectdelalllist(data){
	return serviceAxios({
	    url: '/sys/user/batchRemove',
	    method: 'POST',
	    data
  	})
}
//新增用户
export function adduser(data){
	return serviceAxios({
	    url: '/sys/user/save',
	    method: 'POST',
	    data
  	})
}
//设置所在部门
export function batchEdit(data){
	return serviceAxios({
	    url: '/sys/user/batchEdit',
	    method: 'POST',
	    data
  	})
}
//修改部门名称
export function updatabm(data){
	return serviceAxios({
	    url: '/system/sysDept/update',
	    method: 'POST',
	    data
  	})
}
//设置子部门/system/sysDept/save
export function savszbm(data){
	return serviceAxios({
	    url: '/system/sysDept/save',
	    method: 'POST',
	    data
  	})
}
export function listAcMessage(query) {
  return request({
    url: '/acMessage/list',
    method: 'get',
    params: query
  })
}