
import serviceAxios from '@/common/config/axios'

/*
 请求serviceAxios
 * */

//权限管理人物数据
export function allrolelist(data){
	return serviceAxios({
	    url: '/sys/role/list',
	    method: 'GET',
	    params: data
  	})
}
//根据用户id查询数据
export function findidrole(data){
	return serviceAxios({
	    url: '/sys/user/role/list',
	    method: 'GET',
	    params: data
  	})
}
//新增权限人物菜单
export function addrole(data){
	return serviceAxios({
	    url: '/sys/user/role/batchAdd',
	    method: 'POST',
	    data
  	})
}
//删除权限人物菜单
export function delrole(data){
	return serviceAxios({
	    url: '/sys/user/role/batchRemove',
	    method: 'POST',
	    data
  	})
}