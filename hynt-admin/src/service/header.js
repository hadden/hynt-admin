import serviceAxios from '@/common/config/axios'

/*
 登录请求serviceAxios
 * */

export function menulist(data){
	return serviceAxios({
	    url: '/menu',
	    method: 'GET',
	    params: data
  	})
}
export function listAcMessage(query) {
  return request({
    url: '/acMessage/list',
    method: 'get',
    params: query
  })
}