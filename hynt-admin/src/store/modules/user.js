
const state = {
  isLogin:false,          //初始时候给一个  isLogin=0  表示用户未登录
}

// actions
const actions = {
	changeLogin(context){
		context.commit('changeLogin')
	}
}

// mutations
const mutations = {
  changeLogin(state,data){
        state.isLogin = data;
    }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
